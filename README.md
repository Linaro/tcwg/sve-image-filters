# SVE Image Filters

This program implements basic image filters, both in plain C, and with
intrisics that leverage AArch64's [Scalable Vector Extension](https://developer.arm.com/documentation/101726/0400/Learn-about-the-Scalable-Vector-Extension--SVE-/What-is-the-Scalable-Vector-Extension-).

Using both versions of the code, users can compare the two. The source code,
the assembly produced; the resulting images byte for byte.

It is a companion to the Linaro Connect 2023 talk
["Understanding Scalable Vectors - The Fun Way"](https://resources.linaro.org/en/resource/cuzjd4AvMSr2MKJty5n8q3).

![example filters](examples.jpeg)

Left to right: original, grayscale, invert then average 24x24 pixel blocks.

## Building for SVE

This assumes you are on Linux, it can be adapted as needed.

You can build this natively if you are on a machine with SVE. For example on an
AWS Graviton 3.

For the majority of us, we need to cross compile and run under qemu.

First install a toolchain new enough to support the intrinsics required.
I used `12.2.Rel1` from https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads.

Then install the following:
* `cmake`
* `ninja`
* `qemu-user`

Then you can build with:
```
export PATH=/path/to/toolchain/download/bin/:$PATH
cmake . -G Ninja -DCMAKE_TOOLCHAIN_FILE=aarch64.cmake
ninja
```
To run the program under qemu-user do:
```
qemu-aarch64 ./image_filter example.ppm out.ppm
```
Build, run, view the result side by side with the input:
```
ninja && \
qemu-aarch64 ./image_filter example.ppm out.ppm && \
convert +append example.ppm out.ppm sidebyside.ppm && \
display sidebyside.ppm
```

## Do I have SVE?

On Linux do the following and if you have SVE, you'll see some results.
```
$ cat /proc/cpuinfo | grep sve
Features	: fp <...> sve
```

If you think your hardware should have SVE but doesn't show up as a feature,
check if you need a kernel update to enable it.

## Building without SVE

Install `cmake` and `ninja`. Then configure and build:
```
cmake . -G Ninja
ninja
```

Everything works as before, except you don't need to run the program under
QEMU and it will be native code for your machine (though if you do happen to be
on a machine with SVE, you will still get SVE code).

## Image Format

This tool only supports the `P3` format of [PPM](https://en.wikipedia.org/wiki/Netpbm). This format is an ASCII file with 8 bit channel data.

Using a text based image format means we can easily hand create images and diff them using standard text diff tools.

There is a sample image included here, taken by me (David Spickett) and licensed the same way as everything else contained here.

## Handling PPM Files

[GIMP](https://www.gimp.org/) and [ImageMagick](https://imagemagick.org/index.php) both support the `P3` format. ImageMagick's usual visual diff tools work as you'd expect.

If you install ImageMagick on Linux you will get the `display` utility which can be invoked from the terminal.
Run `display file.ppm` to view an image.

## Configuring Filters

See the `main` function in `main.cpp` for examples of how to add and configure
filters.

## Writing Partial Images

The PPMFile class has the ability to write an image at any time and the data
is always held in the correct 8 bits per channel format ready for writing.

So you can either use a debugger to break and call the write function, or you
can use the following less than elegant hack:
```
  // Finish the current write.
  asm volatile("":::"memory");
  // Number to make a unique, ordered file name for each image.
  static int fnum = 0;
  char buf[10];
  // Assume many digits so that the files are sorted correctly.
  sprintf(buf, "%05d", fnum);
  std::string filename = std::string("outs/filter_") + buf + ".ppm";
  ppm.write(filename.c_str());
  ++fnum;
```
You will need to `mkdir outs` before running the program for this to work.

Put this code pretty much anywhere and you'll get images out. I recommend placing
it at the end of a key loop, just after data is written (see commit
`629510e46860d4a825faf6dec77afebafbfcf096` for an example).

## Creating GIFs

Once you have a folder of images, you can create animations from them. Assuming
you have ImageMagick installed:
```
$ convert -delay 100 -loop 0 outs/*.ppm demo.gif
```
It can help to generate the images at a much smaller resoloution then scale them
back up. That's easy to do:
```
$ mogrify -resize 464x464 -filter point outs/*.ppm
```
GIFs can get very large, so you can also generate an mp4 file. Though you will
need to install FFmpeg for that to work.

## Disclaimers

* This code is not optimal SVE code. It aims to be clear and best display the concepts of scalability.
* This has not been run on real hardware, though it should work there without issue.

