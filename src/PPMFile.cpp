#include <fstream>
#include <iostream>
#include <optional>

#include "PPMFile.h"

PPMFile::PPMFile(std::string format, std::pair<unsigned, unsigned> dimensions,
        unsigned max_channel_value, PixelData pixel_data):
  m_format(format), m_dimensions(dimensions),
  m_max_channel_value(max_channel_value), m_pixel_data(pixel_data) {}

void PPMFile::dump() const {
  std::cout << "Format: " << m_format << "\n";
  std::cout << "Dimensions: " << std::get<0>(m_dimensions) << ", "
            << std::get<1>(m_dimensions) << "\n";
  std::cout << "Max channel value: " << m_max_channel_value << "\n";
}

std::variant<PPMFile, const char*> PPMFile::open_ppm(const char* filepath) {
  std::string line;
  std::ifstream file(filepath);

  if (!file.is_open())
    return "Cannot open file, please check the path.";

  std::optional<std::string> format;
  std::optional<std::pair<unsigned, unsigned>> dimensions;
  std::optional<unsigned> max_channel_value;
  PixelData pixel_data;

  while (getline(file, line)) {
    if (line[0] != '#') {
      if (!format) {
        format = line;
        if (format != "P3")
          return "This tool only supports the P3 format.";
      } else if (!dimensions) {
        size_t space_pos = line.find(' ');
        dimensions = {std::stoi(line.substr(0, space_pos)),
                      std::stoi(line.substr(space_pos+1))};
      } else if (!max_channel_value) {
        max_channel_value = std::stoi(line);
      } else {
        // Is a channel value.
        pixel_data.push_back(std::stoi(line));
      }
    }
  }

  if (format && dimensions && max_channel_value)
    return PPMFile(*format, *dimensions, *max_channel_value, pixel_data);
  else
    return "Couldn't read all image properties, check that it is a valid PPM file.";
}

bool PPMFile::write(const char* filepath) const {
  std::ofstream file(filepath);
  if (!file.is_open())
    return false;

  file << m_format << "\n";
  file << std::get<0>(m_dimensions) << " " << std::get<1>(m_dimensions) << "\n";
  file << m_max_channel_value << "\n";
  for (uint8_t data : m_pixel_data)
    file << (unsigned)data << "\n";

  return true;
}

PixelData& PPMFile::getPixelData() {
  return m_pixel_data;
}

const std::pair<unsigned, unsigned> &PPMFile::getDimensions() const {
  return m_dimensions;
}

unsigned PPMFile::getMaxChannelValue() const {
  return m_max_channel_value;
}

unsigned PPMFile::getNumChannels() const {
  // Assuming P3 format.
  return 3;
}
