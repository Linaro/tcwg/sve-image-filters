#include <iostream>
#include <fstream>
#include <optional>
#include <string>
#include <stdint.h>
#include <vector>
#include <limits>
#include <cstdlib>
#include <tuple>

#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include "PPMFile.h"
#include "Invert.h"
#include "Saturate.h"
#include "Grayscale.h"
#include "Block.h"
#include "Quantise.h"

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Expected 2 arguments. Input file and output file.\n";
    return 1;
  }

  auto infile = argv[1];
  auto outfile = argv[2];

  std::cout << "Applying changes to " << infile << " and writing result to "
            << outfile << ".\n";

  auto ppm_or_err = PPMFile::open_ppm(infile);
  if (std::holds_alternative<const char*>(ppm_or_err)) {
    std::cerr << std::get<const char*>(ppm_or_err) << std::endl;
    return 1;
  }

  auto ppm = std::get<PPMFile>(ppm_or_err);
  ppm.dump();

  // To add a filter, call "apply" with an instance of that filter.
  ppm.apply(Grayscale());

  bool written = ppm.write(outfile);
  if (!written) {
    std::cerr << "Couldn't write output file." << std::endl;
    return 1;
  }

  return 0;
}
