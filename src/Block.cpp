#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include "Block.h"
#include "PPMFile.h"

#include <tuple>
#include <sstream>

std::string Block::describe() const {
    std::stringstream strm;
    strm << "Block average - width " << m_block_width << " height "
         << m_block_height;
    return strm.str();
}

#ifdef __ARM_FEATURE_SVE
// Return an offset sequence assuming that we are starting at pixel_x in the
// current row of the block. The base for this sequence should be the start of
// that row.
static svuint32_t getChannelOffsets(uint32_t start_offset, uint32_t pixel_x,
                                    uint32_t block_width, uint32_t image_width) {
  // A sequence starting at x.
  // x
  // x + 1
  // ...
  svuint32_t offsets = svindex_u32(pixel_x, 1);

  // Divide by the block width W. This shows us where the offsets overflow into
  // the next row.
  //  x    / W
  // (x+1) / W
  // ...
  // Note that we do not predicate here, later the excess elements will be ignored.
  svbool_t pg = svptrue_b32();
  svuint32_t row_offsets = svdiv_m(pg, offsets, block_width);

  // Then multiply that by the image width "I". "I" being how many pixels we have
  // to skip to get to the start of the next row.
  // ( x    / W) * I
  // ((x+1) / W) * I
  // ...
  row_offsets = svmul_m(pg, row_offsets, image_width);

  // Mod the original indexes by the block width "W" to get their offset into
  // their row. We don't have a mod instruction, so do repeated subtractions.
  //  x    % W
  // (x+1) % W
  // ...
  for (svbool_t needs_mod = svcmpge(pg, offsets, block_width);
       // FUNBUG: if you do > rather than >=,  some lines are misaligned by 1 pixel.
       svptest_any(pg, needs_mod); needs_mod = svcmpge(pg, offsets, block_width)) {
    offsets = svsub_m(needs_mod, offsets, block_width);
  }

  // Add the two. To create offsets that are wrapped so that they are all in the block.
  // Essentially row offsets + column offsets.
  // (( x    / W) * I) + ( x    % W)
  // (((x+1) / W) * I) + ((x+1) % W)
  // ...
  offsets = svadd_m(pg, offsets, row_offsets);

  // Multiply by 3 because we want to skip 3 each time as we have 3 channels.
  // 3 * ((( x    / W) * I) + ( x    % W))
  // 3 * ((((x+1) / W) * I) + ((x+1) % W))
  // ...
  offsets = svmul_m(pg, offsets, 3);

  // Add an offset "O" to select R/G/B values from the pixel. 0 for red, 1 for
  // green, 2 for blue.
  // O + (3 * ((( x    / W) * I) + ( x    % W)))
  // O + (3 * ((((x+1) / W) * I) + ((x+1) % W)))
  // ...
  return svadd_m(pg, offsets, start_offset);
}

static svuint32x3_t getChannelsOffsets(uint32_t pixel_x, uint32_t block_width,
                                       uint32_t image_width) {
  return svcreate3(getChannelOffsets(0, pixel_x, block_width, image_width),  // Red
                   getChannelOffsets(1, pixel_x, block_width, image_width),  // Green
                   getChannelOffsets(2, pixel_x, block_width, image_width)); // Blue
}

// Get an offset for the current row in the current block. Add this to
// the start of the channel data to get the base for load/stores.
//
// x             - x origin of block.
// y             - y origin of block.
// current_pixel - Pixel index within the block.
// block_width   - Size of one row of the block.
// image_width   - Width of the overall image.
// num_channels  - Number of colour channels in image.
static size_t getRowOffset(uint32_t x, uint32_t y, uint32_t current_pixel,
                           uint32_t block_width, uint32_t image_width,
                           uint32_t num_channels) {
  uint32_t current_y = y + (current_pixel/block_width);
  // Since we want the start of the row we use x here, not
  // current_pixel % block_width.
  return ((current_y*image_width) + x) * num_channels;
}

void Block::run(PPMFile &ppm) const {
  const auto [image_width, image_height] = ppm.getDimensions();
  unsigned x = 0;
  unsigned y = 0;
  const unsigned num_channels = ppm.getNumChannels();
  uint8_t* pixel_data = ppm.getPixelData().data();

  while (y < image_height) {
    unsigned x_end = x + m_block_width;
    // Edge blocks may not be full sized, so go up to the edges in that case.
    if (x_end >= image_width)
      x_end = image_width;
    unsigned y_end = y + m_block_height;
    if (y_end >= image_height)
      y_end = image_height;

    uint64_t red_total   = 0;
    uint64_t green_total = 0;
    uint64_t blue_total  = 0;

    // Edge blocks can be smaller, so we use the real size not the expected size.
    const uint32_t block_width = x_end - x;
    const unsigned pixels_in_block = block_width * (y_end -y);

    for (unsigned current_pixel = 0; current_pixel < pixels_in_block; current_pixel += svcntw()) {
      svbool_t pg = svwhilelt_b32(current_pixel, pixels_in_block);

      svuint32x3_t channels_offsets = getChannelsOffsets(current_pixel % block_width,
                                                         block_width, image_width);

      uint8_t* base = &pixel_data[getRowOffset(x, y, current_pixel, block_width,
                                               image_width, num_channels)];

      // You cannot gather u8 or u16, so use u32.
      svuint32_t red_values   = svld1ub_gather_offset_u32(pg, base, svget3(channels_offsets, 0));
      svuint32_t green_values = svld1ub_gather_offset_u32(pg, base, svget3(channels_offsets, 1));
      svuint32_t blue_values  = svld1ub_gather_offset_u32(pg, base, svget3(channels_offsets, 2));

      red_total   += svaddv(pg, red_values);
      green_total += svaddv(pg, green_values);
      blue_total  += svaddv(pg, blue_values);
    }

    // Now we have the total of all 3 channels in the block, find the average.
    // FUNBUG: If you just use m_block_width/height, you get an incorrect average
    // because edge blocks may be smaller. It shows up well with the Image Magick
    // compare command.
    svuint32_t red_values   = svdup_u32(red_total   / pixels_in_block);
    svuint32_t green_values = svdup_u32(green_total / pixels_in_block);
    svuint32_t blue_values  = svdup_u32(blue_total  / pixels_in_block);

    for (unsigned current_pixel=0; current_pixel < pixels_in_block; current_pixel += svcntw()) {
      svbool_t pg = svwhilelt_b32(current_pixel, pixels_in_block);
      svuint32x3_t channels_offsets = getChannelsOffsets(current_pixel % block_width,
                                                         block_width, image_width);
      uint8_t* base = &pixel_data[getRowOffset(x, y, current_pixel, block_width,
                                               image_width, num_channels)];

      // Truncating store as u8.
      svst1b_scatter_offset(pg, base, svget3(channels_offsets, 0),   red_values);
      svst1b_scatter_offset(pg, base, svget3(channels_offsets, 1), green_values);
      svst1b_scatter_offset(pg, base, svget3(channels_offsets, 2),  blue_values);
    }

    // Move to next row of the block.
    x += m_block_width;
    // If that puts us off the end of the image.
    if (x >= image_width) {
      // Move to first column of the image.
      x = 0;
      // Move down one block.
      y += m_block_height;
    }
  }
}
#else
using Pixel = std::tuple<uint8_t, uint8_t, uint8_t>;

// Return a new pixel where each channel is set to the average
// for that channel across the block of pixels.
static Pixel average(const std::vector<Pixel> &block) {
  uint64_t red = 0;
  uint64_t green = 0;
  uint64_t blue = 0;
  for (const Pixel& pixel : block) {
    red   += std::get<0>(pixel);
    green += std::get<1>(pixel);
    blue  += std::get<2>(pixel);
  }
  uint64_t num_pixels = block.size();
  return {red/num_pixels, green/num_pixels, blue/num_pixels};
}

void Block::run(PPMFile &ppm) const {
  //Set each block to the average pixel for the pixels in that block of pixels. 
  auto [width, height] = ppm.getDimensions();

  unsigned x = 0;
  unsigned y = 0;

  while (y < height) {
    unsigned x_end = x + m_block_width;
    if (x_end >= width)
      x_end = width;
    unsigned y_end = y + m_block_height;
    if (y_end >= height)
      y_end = height;

    // Collect all pixels from x, y to end_x, end_y.
    std::vector<Pixel> pixels;
    PixelData &pixel_data = ppm.getPixelData();
    for (unsigned current_x = x; current_x < x_end; ++current_x) {
      for (unsigned current_y = y; current_y < y_end; ++current_y) {
        // A pixel has 3 channels that start here.
        size_t idx = ((current_y * width) + current_x) * 3;
        pixels.push_back(Pixel{pixel_data[idx], pixel_data[idx+1],
                               pixel_data[idx+2]});
      }
    }

    auto avg_pixel = average(pixels);

    // Set all pixels in block to that value.
    for (unsigned current_x = x; current_x < x_end; ++current_x) {
      for (unsigned current_y = y; current_y < y_end; ++current_y) {
        size_t idx = ((current_y * width) + current_x) * 3;
        pixel_data[idx]   = std::get<0>(avg_pixel);
        pixel_data[idx+1] = std::get<1>(avg_pixel);
        pixel_data[idx+2] = std::get<2>(avg_pixel);
      }
    }

    // Move to next block.
    x += m_block_width;
    // If that puts us off the end of the image.
    if (x >= width) {
      // Move to first column.
      x = 0;
      // Move down one block.
      y += m_block_height;
    }
  }
}
#endif /* __ARM_FEATURE_SVE */
