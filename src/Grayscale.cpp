#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include "Grayscale.h"
#include "PPMFile.h"

#ifdef __ARM_FEATURE_SVE
static svuint8_t get_avg(svbool_t pg, svuint16_t red, svuint16_t green,
                                svuint16_t blue) {
  svuint16_t total = svadd_m(pg, red, svadd_m(pg, green, blue));
  svfloat16_t divisor = svdup_f16(3);
  svfloat16_t avg_f16 = svcvt_f16_z(pg, total);
  avg_f16 = svdiv_m(pg, avg_f16, divisor);

  // We know that our 3 inputs to the average are inside the u8 range.
  // So we know that the top 8 bits of each u16 are empty and this cast is safe.
  // We will remove the top 8 bits later.
  svuint16_t avg = svcvt_u16_x(pg, avg_f16);
  return svreinterpret_u8(avg);
}

static svuint8_t get_avg_lo(svbool_t pg, svuint8x3_t rgb) {
  svuint16_t red   = svunpklo(svget3(rgb, 0));
  svuint16_t green = svunpklo(svget3(rgb, 1));
  svuint16_t blue  = svunpklo(svget3(rgb, 2));
  pg = svunpklo(pg);
  return get_avg(pg, red, green, blue);
}

static svuint8_t get_avg_hi(svbool_t pg, svuint8x3_t rgb) {
  svuint16_t red   = svunpkhi(svget3(rgb, 0));
  svuint16_t green = svunpkhi(svget3(rgb, 1));
  svuint16_t blue  = svunpkhi(svget3(rgb, 2));
  pg = svunpkhi(pg);
  return get_avg(pg, red, green, blue);
}

void Grayscale::run(PPMFile& ppm) const {
  auto [width, height] = ppm.getDimensions();
  uint64_t num_pixels = width * height;
  unsigned num_channels = ppm.getNumChannels();
  uint8_t* pixel_data = ppm.getPixelData().data();

  // Data is loaded as u8s.
  for (uint64_t idx=0; idx < num_pixels; idx += svcntb()) {
    svbool_t pg = svwhilelt_b8(idx, num_pixels);
    uint8_t* base = &pixel_data[idx * num_channels];

    // This loads 3 vectors one for red, green and blue.
    svuint8x3_t rgb = svld3(pg, base);

    // To do the average we need to widen to u16, then do the work in 2 halves.
    svuint8_t avg_lo = get_avg_lo(pg, rgb);
    svuint8_t avg_hi = get_avg_hi(pg, rgb);

    // At this point our averages are vectors of u8 but only every other u8 is a
    // value that we want to use. To get those we "unzip" the two halves into
    // each other, creating 1 vector with just the valid elements.
    svuint8_t avg = svuzp1(avg_lo, avg_hi);

    svst3(pg, base, svcreate3(avg, avg, avg));
  }
}
#else
void Grayscale::run(PPMFile& ppm) const {
  PixelData& pixel_data = ppm.getPixelData();
  unsigned num_channels = ppm.getNumChannels();
  for (size_t i = 0; i < pixel_data.size(); i+=num_channels) {
     uint8_t &red   = pixel_data[i];
     uint8_t &green = pixel_data[i+1];
     uint8_t &blue  = pixel_data[i+2];

     float new_red   = red;
     float new_green = green;
     float new_blue  = blue;
     float avg = (new_red + new_green + new_blue) / num_channels;

     red   = avg;
     green = avg;
     blue  = avg;
  }
}
#endif /* __ARM_FEATURE_SVE */
