#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include "Invert.h"
#include "PPMFile.h"

#ifdef __ARM_FEATURE_SVE
void Invert::run(PPMFile &ppm) const {
  auto [width, height] = ppm.getDimensions();
  // FUNBUG: If you forget to multiply by the number of channels, only a 1/3
  // of the image gets inverted.
  uint64_t num_channel_data = width * height * ppm.getNumChannels();
  uint8_t* pixel_data = ppm.getPixelData().data();

  for (uint64_t idx=0; idx < num_channel_data; idx += svcntb()) {
    svbool_t pg = svwhilelt_b8(idx, num_channel_data);
    svuint8_t channel_vec = svld1(pg, &pixel_data[idx]);
    channel_vec = svnot_z(pg, channel_vec);
    svst1(pg, &pixel_data[idx], channel_vec);
  }
}
#else
void Invert::run(PPMFile &ppm) const {
  for (uint8_t& data : ppm.getPixelData())
    data = 255 - data;
}
#endif /* __ARM_FEATURE_SVE */
