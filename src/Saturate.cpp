#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include <sstream>

#include "Saturate.h"
#include "PPMFile.h"

std::string Saturate::describe() const {
    std::stringstream strm;
    strm << "Saturate by " << m_change * 100 << "%";
    return strm.str();
}

#ifdef __ARM_FEATURE_SVE
static svuint8_t apply_saturate(svbool_t pg, svuint16_t values, svfloat16_t avg,
                                float change) {
  // Convert the u16 values into f16
  svfloat16_t values_f16 = svcvt_f16_z(pg, values);

  // Find their distance from the average of the whole pixel.
  svfloat16_t distance = svsub_m(pg, avg, values_f16);

  // Multiply that distance by the change.
  svfloat16_t multiplier = svdup_f16(change);
  distance = svmul_m(pg, distance, multiplier);

  // Subtract that from the value to get the new saturated value.
  values_f16 = svsub_m(pg, values_f16, distance);

  // Convert back to u16, which rounds anything <0 to 0.
  values = svcvt_u16_x(pg, values_f16);

  // Apply an upper limit of 255 for when we go back to u8.
  svuint16_t cmp_value = svdup_u16(255);
  svbool_t needs_limit = svcmpgt(pg, values, cmp_value);
  values = svdup_u16_m(values, needs_limit, 255);

  // Return this as a svuint8_t, knowing that the upper byte of each
  // u16 is 0 because of the limit applied above.
  return svreinterpret_u8(values);
}

static svfloat16_t get_avg(svbool_t pg, svuint16_t red, svuint16_t green,
                           svuint16_t blue) {
  svuint16_t total = svadd_m(pg, red, svadd_m(pg, green, blue));
  svfloat16_t avg = svcvt_f16_z(pg, total);
  svfloat16_t divisor = svdup_f16(3);
  return svdiv_m(pg, avg, divisor);
}

void Saturate::run(PPMFile& ppm) const {
  auto [width, height] = ppm.getDimensions();
  uint64_t num_pixels = width * height;
  unsigned num_channels = ppm.getNumChannels();
  uint8_t* pixel_data = ppm.getPixelData().data();

  // Data is loaded as u8s.
  for (uint64_t idx=0; idx < num_pixels; idx += svcntb()) {
    svbool_t pg = svwhilelt_b8(idx, num_pixels);
    uint8_t* base = &pixel_data[idx * num_channels];

    // This loads 3 vectors one for red, green and blue.
    svuint8x3_t rgb_tuple = svld3(pg, base);

    // Widen them from u8 to u16. This produces 2 vectors, one with the lower
    // half of the input, one with the upper half.
    svuint16_t red_lo   = svunpklo(svget3(rgb_tuple, 0));
    svuint16_t green_lo = svunpklo(svget3(rgb_tuple, 1));
    svuint16_t blue_lo  = svunpklo(svget3(rgb_tuple, 2));
    svbool_t pg_lo = svunpklo(pg);
    // Take the average of each pixel.
    svfloat16_t avg_lo = get_avg(pg_lo, red_lo, green_lo, blue_lo);

    svuint16_t red_hi   = svunpkhi(svget3(rgb_tuple, 0));
    svuint16_t green_hi = svunpkhi(svget3(rgb_tuple, 1));
    svuint16_t blue_hi  = svunpkhi(svget3(rgb_tuple, 2));
    svbool_t pg_hi = svunpkhi(pg);
    svfloat16_t avg_hi = get_avg(pg_hi, red_hi, green_hi, blue_hi);

    // Apply the saturation to each half. Then "unzip" them into each other which
    // narrows the u16 into u8. uzp selects the even elements and we know that
    // the odd elements of each will be 0 because we apply a 255 limit. So they
    // can be safely discarded.
    svuint8x3_t results = svcreate3(
      svuzp1(apply_saturate(pg_lo, red_lo,   avg_lo, m_change),
             apply_saturate(pg_hi, red_hi,   avg_hi, m_change)),
      svuzp1(apply_saturate(pg_lo, green_lo, avg_lo, m_change),
             apply_saturate(pg_hi, green_hi, avg_hi, m_change)),
      svuzp1(apply_saturate(pg_lo, blue_lo,  avg_lo, m_change),
             apply_saturate(pg_hi, blue_hi,  avg_hi, m_change))
    );

    svst3(pg, base, results);
  }
}
#else
static uint8_t apply_channel_change(float channel, float avg, float change) {
    float channel_distance = std::abs(channel - avg) * change;
    channel += channel < avg ? - channel_distance : channel_distance;
    if (channel < 0) { channel = 0; }
    if (channel > 255) { channel = 255; }
    return channel;
}

void Saturate::run(PPMFile& ppm) const {
  PixelData& pixel_data = ppm.getPixelData();
  unsigned num_channels = ppm.getNumChannels();
  for (size_t i = 0; i < pixel_data.size(); i+=num_channels) {
     uint8_t &red   = pixel_data[i];
     uint8_t &green = pixel_data[i+1];
     uint8_t &blue  = pixel_data[i+2];

     float new_red   = red;
     float new_green = green;
     float new_blue  = blue;
     float avg = (new_red + new_green + new_blue) / num_channels;

     red   = apply_channel_change(new_red, avg, m_change);
     green = apply_channel_change(new_green, avg, m_change);
     blue  = apply_channel_change(new_blue, avg, m_change);
  }
}
#endif /* __ARM_FEATURE_SVE */
