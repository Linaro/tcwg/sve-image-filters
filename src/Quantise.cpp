#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif /* __ARM_FEATURE_SVE */

#include <sstream>

#include "Quantise.h"
#include "PPMFile.h"

std::string Quantise::describe() const {
    std::stringstream strm;
    strm << "Quantise to " << m_levels << " levels";
    return strm.str();
}

void Quantise::run(PPMFile &ppm) const {
  uint8_t per_level = ppm.getMaxChannelValue() / (m_levels-1);

#ifdef __ARM_FEATURE_SVE
    auto [width, height] = ppm.getDimensions();
    uint64_t num_channel_data = width * height * ppm.getNumChannels();
    uint8_t* pixel_data = ppm.getPixelData().data();

    // FUNBUG: If you use svctnb here instead, you get horizontal stripes in the
    // output where we missed some pixels because we move too many elements forward
    // each time.
    for (uint64_t idx=0; idx < num_channel_data; idx += svcntw()) {
      // Div starts at 32 bit so we zero-extend load and truncating store the result.
      svbool_t pg = svwhilelt_b32(idx, num_channel_data);
      svuint32_t channel_vec = svld1ub_u32(pg, &pixel_data[idx]);
      svuint32_t per_level_vec = svdup_u32(per_level);

      channel_vec = svdiv_m(pg, channel_vec, per_level_vec);
      channel_vec = svmul_m(pg, channel_vec, per_level_vec);

      svst1b_u32(pg, &pixel_data[idx], channel_vec);
    }
#else
    for (uint8_t& data : ppm.getPixelData())
      data -= data % per_level;
#endif /* __ARM_FEATURE_SVE */
  }
