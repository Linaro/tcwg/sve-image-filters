#ifndef BLOCK_H
#define BLOCK_H

#include "Filter.h"
#include <stdint.h>

// Block averaging filter.
//
// Starting from the top right of an image (0, 0), read blocks of width by height
// pixels. Average the invidual channels of all those pixels to get an average
// red, average green, average blue. Make a single pixel from that and set all
// pixels in the block to that pixel.
//
// Width and height do not have to be the same. Neither do they have to be
// factors of the image's width or height. If there is not enough image
// remaining for a full block, we will use what is left.

class Block : public Filter {
public:
  Block(unsigned width, unsigned height):
    m_block_width(width), m_block_height(height) {}

  void run(PPMFile &ppm) const override;

  std::string describe() const override;

private:
  uint64_t m_block_width;
  uint64_t m_block_height;
};

#endif /* ifndef BLOCK_H */
