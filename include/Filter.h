#ifndef FILTER_H
#define FILTER_H

#include <string>

class PPMFile;

struct Filter {
  // Apply the filter to the ppm file.
  virtual void run(PPMFile& ppm) const = 0;

  // Produce a string saying what the filter is and how it is configured.
  virtual std::string describe() const = 0;
};

#endif /* ifndef FILTER_H */
