#ifndef GRAYSCALE_H
#define GRAYSCALE_H

#include "Filter.h"

// A grayscale filter, also known as removing all saturation from an image.
// There are many ways to do that, the method used here is to set all channels
// in the pixel to the average of the R/G/B channels in that pixel (that average
// is also known as the intensity when in HSI space).
struct Grayscale : Filter {
  void run(PPMFile& ppm) const override;

  std::string describe() const override {
    return "Grayscale";
  }
};

#endif /* ifndef GRAYSCALE_H */
