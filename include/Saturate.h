#ifndef SATURATE_H
#define SATURATE_H

#include "Filter.h"

// Increase the separation of each colour, from the average of all the colours
// in the pixel.
//
// Note: This is not a correct saturation. The correct way to do this is to
// convert RGB into HSL/HSI (hue / saturation / luminance or intensity),
// change saturation and convert back to RGB. This is close visually but
// not correct if you compare the values to a correct filter.
class Saturate : public Filter {
public:
  // Where 1 = 100% increase, -1 = 100% decrease. Pass 0 to keep the same levels.
  // Value should be in the range -1 to some positive number.
  Saturate(float change): m_change(change) {}

  void run(PPMFile& ppm) const override;

  std::string describe() const override;

private:
  float m_change;
};

#endif /* ifndef SATURATE_H */
