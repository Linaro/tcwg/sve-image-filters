#ifndef QUANTISE_H
#define QUANTISE_H

#include "Filter.h"

// Quantise an image to a certain number of levels.
//
// Take each channel value and quantise it as if the channel was divided into
// "levels" number of levels. For instance 2 levels would be 0 and 255. 3 would
// give you a 255/2 level.
//
// The effect is best understood by applying a grayscale first, but it is not
// required.
//
// Note that this rounds *down* to the nearest level. Meaning that for 2 levels
// anything < 255 will become 0. Which can be confusing if you have an image
// that has no white pixels in it because you'll get a solid black image.

class Quantise : public Filter {
public:
  Quantise(unsigned levels): m_levels(levels) {}

  void run(PPMFile &ppm) const override;

  std::string describe() const override;

private:
  unsigned m_levels;
};

#endif /* ifndef QUANTISE_H */
