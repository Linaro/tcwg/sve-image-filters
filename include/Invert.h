#ifndef INVERT_H
#define INVERT_H

#include "Filter.h"

// Invert the colours of an image.
//
// This works by taking each channel's value and inverting that around the
// middle of the channel range. So if red was 255, it would become 0. If it
// were 254 it would become 1.

struct Invert : Filter {
  void run(PPMFile &ppm) const override;

  std::string describe() const override {
    return "Invert";
  }
};

#endif /* ifndef INVERT_H */
