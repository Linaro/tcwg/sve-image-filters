#ifndef PPM_FILE_H
#define PPM_FILE_H

#include <vector>
#include <string>
#include <utility>
#include <variant>
#include <iostream>

#include "Filter.h"

using PixelData = std::vector<uint8_t>;

class PPMFile {
public:
  static std::variant<PPMFile, const char*> open_ppm(const char* filepath);

  bool write(const char* filepath) const;

  void apply(const Filter &filter) {
    static int filter_number = 0;
    std::cout << filter_number << ". " << filter.describe() << "\n";
    filter.run(*this);
    filter_number++;
  }

  PixelData& getPixelData();

  const std::pair<unsigned, unsigned> &getDimensions() const;

  unsigned getMaxChannelValue() const;

  unsigned getNumChannels() const;

  void dump() const;

private:
  PPMFile(std::string format, std::pair<unsigned, unsigned> dimensions,
          unsigned max_channel_value, PixelData pixel_data);

  std::string m_format;
  std::pair<unsigned, unsigned> m_dimensions;
  unsigned m_max_channel_value;
  PixelData m_pixel_data;
};

#endif /* ifndef PPM_FILE_H */
