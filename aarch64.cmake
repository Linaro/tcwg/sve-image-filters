set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR AArch64)

set(CMAKE_C_COMPILER aarch64-none-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER aarch64-none-linux-gnu-g++)

# Static linking means we don't have to give qemu-user a library path.
set(CMAKE_CXX_FLAGS "-march=armv8.2-a+sve -O3 -static")
set(CMAKE_C_FLAGS ${CMAKE_CXX_FLAGS})
